class Dashboard::OrdersController < ApplicationController
  layout 'dashboard'

  before_action :authenticate_user!
  before_action :authenticate_admin!

  def index
    # lock orders for the issue duplicate item on next pages
    @orders = Order.all.lock_data(current_user).includes(:user)
    @orders = @orders.search_by(status: params[:status],  user_email: params[:user_email]) if params[:status] != 'status' || params[:user_email].present?
    @orders = @orders.page(params[:page] || 1).per(params[:per] || 10)
  end

  def edit
    @order = Order.find_by(id: params[:id])
  end

  def update
    @order = Order.find_by(id: params[:id])
    @order.update(order_params)

    render 'edit'
  end

  private

  def order_params
    params.require(:order).permit(:status)
  end
end
