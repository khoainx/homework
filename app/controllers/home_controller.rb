class HomeController < ApplicationController

  def index
    if current_user.present?
      if current_user.admin?
        redirect_to dashboard_path
      else
        redirect_to dashboard_path, flash: { error: "Sorry, you're not authorized to access this page." }
      end
      # redirect_to(dashboard_path)
    else
      redirect_to new_user_session_path, flash: { error: "Please login." }
    end
  end
end
