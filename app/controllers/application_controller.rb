class ApplicationController < ActionController::Base
  def authenticate_admin!
    redirect_to dashboard_path, flash: { alert: "Sorry, you're not authorized to access this page." } unless current_user&.admin?
  end
end
