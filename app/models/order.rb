class Order < ApplicationRecord
  belongs_to :user, foreign_key: 'user_id'

  enum status: [:pending_payment, :partially_paid, :paid, :canceled, :refunded, :expired]
  validates_presence_of :status

  def self.search_by(search_params)
    orders = search_params[:user_email].present? ? includes(:user).where(users: { email: search_params[:user_email] }) : self
    orders = orders.where(status: search_params[:status]) if search_params[:status].present? && Order.statuses.keys.include?(search_params[:status])

    orders
  end

  def self.lock_data(user)
    user.update!(additional_info: { last_lock_order: Time.current })
    where("created_at <= ?", user.additional_info[:last_lock_order] || Time.current)
  end
end
