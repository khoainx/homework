# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts 'Creating users and orders'
User.create!(email: 'admin@homework.com', password: 'admin123', admin: true)

(0..9).each do |i|
  user = User.create(email: "user_#{i + 1}@homework.com", password: '123123')

  (0..9).each do |_i|
    Order.create(user_id: user.id, product_name: "product_#{rand(1..3)}", status: Order.statuses.keys[rand(0..5)])
  end
end

puts 'Done'
