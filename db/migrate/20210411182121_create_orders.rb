class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :status, index: true
      t.string :product_name
      t.integer :user_id

      t.timestamps
    end
  end
end
