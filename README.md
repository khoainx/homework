## Overview

## Quick Start

Clone the repo and bundle

  git clone https://gitlab.com/khoainx/homework.git
  cd homework
  bundle install


Setting up `database.yml` based on your needs. (Use the default settings if you're not sure what to change)

## Usage

  - Create DB:
    rake db:create
    rake db:migrate
  - Generate sample data:
    rake db:seed

  - Continuous testing:
    + Access `localhost:3000` for sign in

  - Rspec:
    + Bundle exec rspec spec

## Test Accounts

- Admin email: admin@homework.com
- Password: admin123
